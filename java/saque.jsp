<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <% if(session.getAttribute("user") != null) {%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
<meta http-equiv='X-UA-compatible' content='IE=edge, chrome=1'/>
<link rel='stylesheet' href='uikit/css/uikit.min.css' />
<link rel='stylesheet' href='uikit/css/css2.css' />
<title>Money</title>
</head>
<body>
<div class="uk-text-center" uk-grid style="margin-left: 30%;">
		<div class="uk-width-1-2@m">
			<h1 class="uk-heading-bullet">Histórico de Saques</h1>
			<table class="uk-table uk-table-striped">
			    <thead>
			        <tr>
			            <th class="uk-text-center">Valor</th>
			            <th class="uk-text-center">Tipo de Saque</th>
			        </tr>
			    </thead>
			    <tbody>
			    	
			    </tbody>
			</table>
		</div>
	</div>
	<script src="uikit/js/uikit.min.js"></script>
	<script src="uikit/js/uikit-icons.min.js"></script>

</body>
</html>
<%}else{
	response.sendRedirect("index.jsp");
}%>