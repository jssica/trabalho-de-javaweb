<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <% if(session.getAttribute("user") != null) {%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
<meta http-equiv='X-UA-compatible' content='IE=edge, chrome=1'/>
<link rel='stylesheet' href='uikit.min.css' />
<link rel='stylesheet' href='css2.css' />
<title>Deposito</title>
</head>
<body>
<div class='uk-child-width-1-1@s uk-flex uk-child-width-1-1@m uk-position-center' uk-grid='masonry: true'>
<h1 class="uk-heading-bullet">Deposito</h1>
		<form action='backSalario' method='post'>
			<div class='uk-margin'>
		        <label>Usuário:</label>
		        <div class='uk-inline'>
		            <input name='user' id='user' type='text' value='' class='uk-input' required>
		        </div><br>
		    </div>
			<div class='uk-margin'>
			    <label>Valor:</label>
			    <div class='uk-inline'>
			        <input  name='salario' id='salario' class='uk-input' type='float'>
			    </div>
			</div>
			<div class="uk-margin">
	            <button class='uk-button uk-button-default'>Registrar</button>
	        </div>
		</form>
	<script src="uikit.min.js"></script>
	<script src="uikit-icons.min.js"></script>
</body>
</html>
<%}else{
	response.sendRedirect("index.jsp");
}%>