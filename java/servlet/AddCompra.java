package servlet;
import controle.ComprasControle;
import modelo.Compras;
import modelo.Conta;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddCompra
 */
@WebServlet(name = "addCompra", urlPatterns = { "/addCompra" })
public class AddCompra extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCompra() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String user = request.getParameter("user");
		String produto = request.getParameter("produto");
		Float valor = Float.parseFloat(request.getParameter("valor"));
		String dataCompra = request.getParameter("datacompra");
		String numero = request.getParameter("cartao");
		ComprasControle aux = new ComprasControle();
		aux.cadastro(user, produto, valor, dataCompra, numero);
		
	}

}
