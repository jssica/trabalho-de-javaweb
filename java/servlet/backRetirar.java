package servlet;

import java.io.IOException;
import javax.servlet.ServletException; 
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controle.ContaControle;
import modelo.Conta;

@WebServlet("/BackRetirar")
public class backRetirar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public backRetirar() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("pagamentoDeCartao.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String user = request.getParameter("user"); 
	Float dinheiro = Float.parseFloat(request.getParameter("dinheiro"));
	Conta inst = new Conta();
	inst.setUser(user);
	inst.setCapital(dinheiro);
	ContaControle aux = new ContaControle();
	aux.retirarDinheiro(inst);
		response.getWriter().print("<script>alert('deu certo');window.location.href='index.jsp'</script>");
	}
}
