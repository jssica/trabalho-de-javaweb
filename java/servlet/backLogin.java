package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Conta;
import controle.ContaControle;
import javax.servlet.http.HttpSession;
/**
 * Servlet implementation class backLogin
 */
@WebServlet("/backLogin")
public class backLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public backLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//int id = Integer.parseInt(request.getParameter("id"));
		String user = request.getParameter("user");
		String senha = request.getParameter("senha");
		Conta dono = new Conta();
		String usuario = dono.getUser();
		String pwd = dono.getSenha();
		dono.setUser(user);
		dono.setSenha(senha);
		HttpSession sessao = request.getSession();
		sessao.setAttribute("user", dono); 
		sessao.setAttribute("iduser", dono.getId());
		boolean add = new ContaControle().logar(dono);
		if(add) {
//			request.getRequestDispatcher("index.jsp").forward(request, response);
			response.getWriter().print("<script>alert('deu certo');window.location.href='home.jsp'</script>");
		}else {
			response.getWriter().print("<script>alert('deu erro');window.location.href='login.jsp'</script>");
	}
	}

}
