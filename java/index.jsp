<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Money | Login</title>
		<link rel='stylesheet' href='uikit.min.css' />
	    <link rel='stylesheet' href='css.css' />
</head>
<body id ="menu">
<div style='background-image: url(imagem.jpg);'>
		<div class='uk-child-width-1-1@s uk-flex uk-child-width-1-1@m uk-position-center'>
			<div  id='lugar'>
			    <div>
		            <h1 class='uk-heading-bullet'>Login</h1>
					<form action='backLogin' method='post'>
		            	<div class='uk-align-left'>
			                <div class='uk-margin'>
			                    <label>Nome:</label>
			                    <div class='uk-inline'>
			                        <span class='uk-form-icon' uk-icon='icon: user'></span>
			                      	<input name='user' id='user' class='uk-input' type='text' required>
			                    </div><br>
			                    <div class='uk-margin'>
				                    <label>Senha:</label>
				                    <div class='uk-inline'>
				                        <span class='uk-form-icon' uk-icon='icon: lock'></span>
				                        <input  name='senha' id='senha' class='uk-input' type='password' required>
				                    </div>
				                </div>
			                </div>
			                <center>
		                    	<button class='uk-button uk-button-default'>Logar</button>
		                    </center>
		                </div>
		                <div class='uk-align-center'>
		                	<center>Ainda n�o tem seu cadastro? <a href='frontcadastro' id='cadastro'><br>Cadastre-se j�!</a></center>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		</div>
	<script src='uikit.min.js'></script>
	<script src='uikit-icons.min.js'></script>
	<script src='jquery.js'></script>
	<script src='ajax2.js'></script>

</body>
</html>