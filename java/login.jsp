<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
<meta http-equiv='X-UA-compatible' content='IE=edge, chrome=1'/>
<link rel='stylesheet' href='uikit.min.css' />
<link rel='stylesheet' href='css2.css' />

<title>Login</title>
</head>
<%if(session.getAttribute("user") != null){
	response.sendRedirect("home.jsp");
}
%>
<body>
		<body id='menu'>
		<div>
		<div class='uk-child-width-1-1@s uk-flex uk-child-width-1-1@m uk-position-center'>
			<div  id='lugar'>
			    <div>
		            <h1 class='uk-heading-bullet'>Login</h1>
					<form action='backLogin' method='post'>
		            	<div class='uk-align-left'>
			                <div class='uk-margin'>
			                    <label>Nome:</label>
			                    <div class='uk-inline'>
			                        <span class='uk-form-icon' uk-icon='icon: user'></span>
			                      	<input name='user' id='user' class='uk-input' type='text' required>
			                    </div><br>
			                    <div class='uk-margin'>
				                    <label>Senha:</label>
				                    <div class='uk-inline'>
				                        <span class='uk-form-icon' uk-icon='icon: lock'></span>
				                        <input  name='senha' id='senha' class='uk-input' type='password' required>
				                    </div>
				                </div>
			                </div>
			                <center>
		                    	<button class='uk-button uk-button-default'>Logar</button>
		                    </center>
		                </div>
		                <div class='uk-align-center'>
		                	<center>Ainda não tem seu cadastro? <a href='#' id='cadastro'><br>Cadastre-se já!</a></center>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		</div>
	<script src='uikit.min.js'></script>
	<script src='uikit-icons.min.js'></script>
	<script src='jquery.js'></script>
	<script src='ajax2.js'></script>
</body>
</html>